/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transactions;

import csg.data.OfficeHoursData;
import javafx.scene.control.ComboBox;
import jtps.jTPS_Transaction;

/**
 *
 * @author New User
 */
public class OHChangeTimeRange_Transaction implements jTPS_Transaction{
    OfficeHoursData data;
    String oldStartTime;
    String oldEndTime;
    String newStartTime;
    String newEndTime;
    ComboBox startTimeBox;
    ComboBox endTimeBox;
    public OHChangeTimeRange_Transaction(OfficeHoursData data, String oldStartTime, String newStartTime, String oldEndTime, 
                    String newEndTime, ComboBox startTimeBox, ComboBox endTimeBox) {
        this.data = data;
        this.oldStartTime = oldStartTime;
        this.oldEndTime = oldEndTime;
        this.newStartTime = newStartTime;
        this.newEndTime = newEndTime;
        this.startTimeBox = startTimeBox;
        this.endTimeBox = endTimeBox;
    }

    @Override
    public void doTransaction() {
       data.changeTimeRange(newStartTime, newEndTime);
//       startTimeBox.getSelectionModel().select(newStartTime);
//       endTimeBox.getSelectionModel().select(newEndTime);
    }

    @Override
    public void undoTransaction() {
        data.changeTimeRange(oldStartTime, oldEndTime);
//        startTimeBox.getSelectionModel().select(oldStartTime);
//        endTimeBox.getSelectionModel().select(oldEndTime);
    }
}
