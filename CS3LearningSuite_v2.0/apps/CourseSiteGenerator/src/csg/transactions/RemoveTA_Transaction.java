package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.OfficeHoursData;
import csg.data.TeachingAssistantPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class RemoveTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype ta;
    
    public RemoveTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA) {
        data = initData;
        ta = initTA;
    }

    @Override
    public void doTransaction() {
        data.removeTA(ta);        
    }

    @Override
    public void undoTransaction() {
        data.addTA(ta);
    }
}
