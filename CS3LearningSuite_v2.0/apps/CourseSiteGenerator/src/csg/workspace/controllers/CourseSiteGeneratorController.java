package csg.workspace.controllers;

import static csg.CourseSiteGeneratorPropertyType.OH_FOOLPROOF_SETTINGS;
import static csg.CourseSiteGeneratorPropertyType.OH_NO_TA_SELECTED_CONTENT;
import static csg.CourseSiteGeneratorPropertyType.OH_NO_TA_SELECTED_TITLE;
import static csg.CourseSiteGeneratorPropertyType.OH_TA_EDIT_DIALOG;
import csg.data.OfficeHoursData;
import csg.data.TAType;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.data.TimeSlot.DayOfWeek;
import csg.transactions.AddTA_Transaction;
import csg.transactions.EditTA_Transaction;
import csg.transactions.ToggleOfficeHours_Transaction;
import csg.workspace.dialogs.TADialog;
import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import javafx.collections.ObservableList;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_END_TIME_COMBO_BOX;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_NAME_TEXT_FIELD;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_START_TIME_COMBO_BOX;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_TABLE_VIEW;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_TAS_TABLE_VIEW;
import static csg.CourseSiteGeneratorPropertyType.CSG_OH_INVALID_TIME_RANGE_CONTENT;
import static csg.CourseSiteGeneratorPropertyType.CSG_SITE_FAVICON_IMAGE_VIEW;
import static csg.CourseSiteGeneratorPropertyType.CSG_SITE_LEFTE_FOOTER_IMAGE_VIEW;
import static csg.CourseSiteGeneratorPropertyType.CSG_SITE_NAVBAR_IMAGE_VIEW;
import static csg.CourseSiteGeneratorPropertyType.CSG_SITE_RIGHT_FOOTER_IMAGE_VIEW;
import csg.transactions.OHChangeTimeRange_Transaction;
import csg.transactions.RemoveTA_Transaction;
import static djf.AppPropertyType.LOAD_IMAGE_TITLE;

import static djf.AppPropertyType.SAVE_WORK_TITLE;
import java.io.File;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableRow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author McKillaGorilla
 */
public class CourseSiteGeneratorController {

    CourseSiteGeneratorApp app;

    public CourseSiteGeneratorController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(CSG_OFFICE_HOURS_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        TAType type = data.getSelectedType();
        if (data.isLegalNewTA(name, email)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type);
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }

    public void processRemoveTA() {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        if (ta != null) {
            RemoveTA_Transaction removeTATransaction = new RemoveTA_Transaction(data, ta);
            app.processTransaction(removeTATransaction);
        }
            taTableView.refresh();
            officeHoursTableView.refresh();
    }
    public void processVerifyTA() {

    }

    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }

    public void processEditTA() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        if (data.isTASelected()) {
            TeachingAssistantPrototype taToEdit = data.getSelectedTA();
            TADialog taDialog = (TADialog)app.getGUIModule().getDialog(OH_TA_EDIT_DIALOG);
            taDialog.showEditDialog(taToEdit);
            TeachingAssistantPrototype editTA = taDialog.getEditTA();
            if (editTA != null) {
                EditTA_Transaction transaction = new EditTA_Transaction(taToEdit, data, editTA.getName(), editTA.getEmail(), editTA.getType());
                app.processTransaction(transaction);
            }
        }
        officeHoursTableView.refresh();
        taTableView.refresh();
    }

    public void processSelectAllTAs() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        data.selectTAs(TAType.All);
    }

    public void processSelectGradTAs() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        data.selectTAs(TAType.Graduate);
    }

    public void processSelectUndergradTAs() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        data.selectTAs(TAType.Undergraduate);
    }

    public void processSelectTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.refresh();
    }
    
    public void processOHSelectTime(String oldStartTime, String newStartTime, String oldEndTime, String newEndTime) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        ComboBox startTimeBox = (ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBO_BOX);
        ComboBox endTimeBox = (ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBO_BOX);
//        String startTime = (String)startTimeBox.getValue();
//        String endTime = (String)endTimeBox.getValue();
         String startTimeTerm = newStartTime.substring(newStartTime.length() - 2);
        int startColonIndex = newStartTime.indexOf(":");
        int startTimeMin = Integer.valueOf(newStartTime.substring(startColonIndex + 1, startColonIndex + 3));
        int startTimeHour = Integer.valueOf(newStartTime.substring(0, startColonIndex));
        
        String endTimeTerm = newEndTime.substring(newEndTime.length() - 2);
        int endColonIndex = newEndTime.indexOf(":");
        int endTimeMin = Integer.valueOf(newEndTime.substring(endColonIndex + 1, endColonIndex + 3));
        int endTimeHour = Integer.valueOf(newEndTime.substring(0, endColonIndex));
//        boolean validTimeRange = true;
//        if (startTimeTerm.equals("am") && endTimeTerm.equals("am")) {
//                if (startTimeHour > endTimeHour || (startTimeHour == endTimeHour && startTimeMin >= endTimeMin)) {
//                    validTimeRange = false;
//                }
//        }
//        else if (startTimeTerm.equals("pm") && endTimeTerm.equals("am")) {
//             validTimeRange = false;
//        }
//        else if (startTimeTerm.equals("pm") && endTimeTerm.equals("pm")) {
//            if ((startTimeHour == 12 && endTimeHour == 12) || (startTimeHour != 12 && endTimeHour != 12) 
//                        && (startTimeMin >= endTimeMin)) {
//                validTimeRange = false;
//            }
//            else if (startTimeHour != 12 && endTimeHour == 12) {
//                validTimeRange = false;
//            }
//        }
//        if (!validTimeRange) {
//            AppDialogsFacade.showMessageDialog(gui.getWindow(), "",CSG_OH_INVALID_TIME_RANGE_CONTENT);
//            startTimeBox.getSelectionModel().select(oldStartTime);
//            endTimeBox.getSelectionModel().select(oldEndTime);
//        }
//        else {
            OHChangeTimeRange_Transaction transaction = new OHChangeTimeRange_Transaction(data, oldStartTime, newStartTime, 
                            oldEndTime, newEndTime, startTimeBox, endTimeBox);
            app.processTransaction(transaction);
//        }
    }
    
    public void processFaviconButton() {
        AppGUIModule gui = app.getGUIModule();
        ImageView imageView = (ImageView)gui.getGUINode(CSG_SITE_FAVICON_IMAGE_VIEW);
        File file = AppDialogsFacade.showImageDialog(app.getGUIModule().getWindow(), LOAD_IMAGE_TITLE);
        if (file != null) 
            imageView.setImage(new Image(file.toURI().toString()));
    }
    public void processNavbarButton() {
        AppGUIModule gui = app.getGUIModule();
        ImageView imageView = (ImageView)gui.getGUINode(CSG_SITE_NAVBAR_IMAGE_VIEW);
        File file = AppDialogsFacade.showImageDialog(app.getGUIModule().getWindow(), LOAD_IMAGE_TITLE);
        if (file != null)
            imageView.setImage(new Image(file.toURI().toString()));
    }
    public void processLefterFooterButton() {
        AppGUIModule gui = app.getGUIModule();
        ImageView imageView = (ImageView)gui.getGUINode(CSG_SITE_LEFTE_FOOTER_IMAGE_VIEW);
        File file = AppDialogsFacade.showImageDialog(app.getGUIModule().getWindow(), LOAD_IMAGE_TITLE);
        if (file != null)
            imageView.setImage(new Image(file.toURI().toString()));
    }
    public void processRightFooterButton() {
        AppGUIModule gui = app.getGUIModule();
        ImageView imageView = (ImageView)gui.getGUINode(CSG_SITE_RIGHT_FOOTER_IMAGE_VIEW);
        File file = AppDialogsFacade.showImageDialog(app.getGUIModule().getWindow(), LOAD_IMAGE_TITLE);
        if (file != null)
            imageView.setImage(new Image(file.toURI().toString()));
    }
}