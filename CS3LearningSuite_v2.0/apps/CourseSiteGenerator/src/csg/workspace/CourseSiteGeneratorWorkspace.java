package csg.workspace;

import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorPropertyType;
import static csg.CourseSiteGeneratorPropertyType.*;
import csg.data.MeetingTimes;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.workspace.controllers.CourseSiteGeneratorController;
import csg.workspace.controllers.OfficeHoursController;
import csg.workspace.dialogs.TADialog;
import csg.workspace.foolproof.OfficeHoursFoolproofDesign;
import static csg.workspace.style.CSGStyle.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author McKillaGorilla
 */
public class CourseSiteGeneratorWorkspace extends AppWorkspaceComponent {
    private Button siteOHTextButton;
    private TextArea siteTextArea;
    private Button descriptionButton;
    private Button topicsButton; 
    private  Button prerequisitesButton;
    private Button outcomesButton;    
    private Button textbooksButton;
    private Button gcButton;       
    private Button gnButton;       
    private Button adButton;       
    private Button saButton;
    private TextArea desTextArea;
    private TextArea preTextArea;
    private TextArea topTextArea;
    private TextArea outTextArea;        
    private TextArea texTextArea;        
    private TextArea gcTextArea;
    private TextArea gnTextArea;
    private TextArea adTextArea;
    private TextArea saTextArea;
    public CourseSiteGeneratorWorkspace(CourseSiteGeneratorApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // 
        initFoolproofDesign();

        // INIT DIALOGS
        initDialogs();
    }

    private void initDialogs() {
        TADialog taDialog = new TADialog((CourseSiteGeneratorApp) app);
        app.getGUIModule().addDialog(OH_TA_EDIT_DIALOG, taDialog);
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppGUIModule gui = app.getGUIModule();  
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder ohBuilder = app.getGUIModule().getNodesBuilder();

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        ohBuilder.buildLabel(CourseSiteGeneratorPropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);
        HBox typeHeaderBox = ohBuilder.buildHBox(OH_GRAD_UNDERGRAD_TAS_PANE, tasHeaderBox, CLASS_OH_RADIO_BOX, ENABLED);
        ToggleGroup tg = new ToggleGroup();
        ohBuilder.buildRadioButton(OH_ALL_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, true);
        ohBuilder.buildRadioButton(OH_GRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);
        ohBuilder.buildRadioButton(OH_UNDERGRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_CENTERED_COLUMN);
        TableColumn typeColumn = ohBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("Name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(2.0 / 5.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));

        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, !ENABLED);

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        setupOfficeHoursColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "startTime");
        setupOfficeHoursColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "endTime");
        setupOfficeHoursColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "monday");
        setupOfficeHoursColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "tuesday");
        setupOfficeHoursColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "wednesday");
        setupOfficeHoursColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "thursday");
        setupOfficeHoursColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "friday");

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        /*********************************************************************************************************************************************************/
         TabPane tabPane = ohBuilder.buildTabPane(CSG_TABPANE, null, CLASS_CSG_TAB_PANE , ENABLED);
         Tab siteTab = new Tab("Site");
         Tab syllabusTab = new Tab("Syllabus");
         Tab mtsTab = new Tab("Meeting Times");
         Tab ohTab = new Tab("Office Hours");
         Tab scheduleTab = new Tab("Schedule");
//         TextTab siteTab = ohBuilder.buildTextTab();
         siteTab.getStyleClass().add(CLASS_CSG_TABS );
         syllabusTab.getStyleClass().add(CLASS_CSG_TABS );
         mtsTab.getStyleClass().add(CLASS_CSG_TABS );
         ohTab.getStyleClass().add(CLASS_CSG_TABS );
         scheduleTab.getStyleClass().add(CLASS_CSG_TABS );
         siteTab.setClosable(false);
         syllabusTab.setClosable(false);
         mtsTab.setClosable(false);
         ohTab.setClosable(false);
         scheduleTab.setClosable(false);
         tabPane.getTabs().add(siteTab);
         tabPane.getTabs().add(syllabusTab);
         tabPane.getTabs().add(mtsTab);
         tabPane.getTabs().add(ohTab);
         tabPane.getTabs().add(scheduleTab);
         Stage stage = gui.getWindow();
         tabPane.tabMaxWidthProperty().bind(stage.widthProperty().multiply(1.0 / 6.0));
         tabPane.tabMinWidthProperty().bind(stage.widthProperty().multiply(1.0 / 6.0));
         //*
         //*SITE PANE
         //*
         ScrollPane siteScrollPane = new ScrollPane();
         
         VBox sitePane = ohBuilder.buildVBox(CSG_SITE_PANE, null, CLASS_CSG_MAIN_PANE, ENABLED);
         siteScrollPane.setContent(sitePane);
         sitePane.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
         sitePane.prefHeightProperty().bind(stage.widthProperty());
         //
         //Banner Pane
         //
         VBox bannerPane = ohBuilder.buildVBox(CSG_SITE_BANNER_PANE, sitePane, CLASS_CSG_PANE , ENABLED);
         HBox bannerHeaderPane = ohBuilder.buildHBox(CSG_SITE_BANNER_HEADER_PANE, bannerPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_BANNER_HEADER_LABEL,bannerHeaderPane, CLASS_CSG_HEADER_LABEL, ENABLED);
         
         HBox subjectPane = ohBuilder.buildHBox(CSG_SITE_SUBJECT_PANE, bannerPane, CLASS_CSG_BOX , ENABLED);
         ohBuilder.buildLabel(CSG_SITE_SUBJECT_LABEL, subjectPane, CLASS_CSG_SITE_SUBJECT_TEXT_LABEL  , ENABLED);
         ohBuilder.buildComboBox(CSG_SITE_SUBJECT_COMBO_BOX, CSG_SITE_SUBJECT_OPTIONS, 
                        CSG_SITE_SUBJECT_DEFAULT_OPTION, subjectPane,CLASS_CSG_COMBO_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_NUMBER_LABEL, subjectPane, CLASS_CSG_SITE_NUMBER_TEXT_LABEL, ENABLED);
         ohBuilder.buildComboBox(CSG_SITE_NUMBER_COMBO_BOX, CSG_SITE_NUMBER_OPTIONS, 
                        CSG_SITE_NUMBER_DEFAULT_OPTION, subjectPane, CLASS_CSG_COMBO_BOX, ENABLED);
         
         HBox semesterPane = ohBuilder.buildHBox(CSG_SITE_SEMESTER_PANE, bannerPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_SEMESTER_LABEL, semesterPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildComboBox(CSG_SITE_SEMESTER_COMBO_BOX, CSG_SITE_SEMESTER_OPTIONS, 
                        CSG_SITE_SEMESTER_DEFAULT_OPTION, semesterPane, CLASS_CSG_COMBO_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_YEAR_LABEL, semesterPane, CLASS_CSG_SITE_YEAR_TEXT_LABEL, ENABLED);
         ohBuilder.buildComboBox(CSG_SITE_YEAR_COMBO_BOX, CSG_SITE_YEAR_OPTIONS,  
                        CSG_SITE_YEAR_DEFAULT_OPTION, semesterPane, CLASS_CSG_COMBO_BOX, ENABLED);
         
         HBox titlePane = ohBuilder.buildHBox(CSG_SITE_TITLE_PANE, bannerPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_TITLE_LABEL, titlePane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildTextField(CSG_SITE_TITLE_TEXT_FIELD, titlePane, CLASS_CSG_TEXT_FIELD, ENABLED);
         
         HBox exportPane = ohBuilder.buildHBox(CSG_SITE_EXPORT_DIR_PANE, bannerPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_EXPORT_DIR_LABEL, exportPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_EXPORT_DIR_PATH, exportPane, CLASS_CSG_SITE_EXPORT_DIR_PATH_TEXT, ENABLED);
         
         //
         //Pages Pane
         //
         VBox pagesPane = ohBuilder.buildVBox(CSG_SITE_PAGES_PANE, sitePane, CLASS_CSG_PANE, ENABLED);
         HBox pagesHeaderPane = ohBuilder.buildHBox(CSG_SITE_PAGES_HEADER_PANE, pagesPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_PAGES_HEADER_LABEL, pagesHeaderPane, CLASS_CSG_HEADER_LABEL, ENABLED);
         ohBuilder.buildCheckBox(CSG_SITE_PAGES_HOME_CHECK_BOX, pagesHeaderPane, CLASS_CSG_SITE_PAGES_CHECK_BOX, ENABLED);
         ohBuilder.buildCheckBox(CSG_SITE_PAGES_SYLLABUS_CHECK_BOX, pagesHeaderPane, CLASS_CSG_SITE_PAGES_CHECK_BOX , ENABLED);
         ohBuilder.buildCheckBox(CSG_SITE_PAGES_SCHEDULE_CHECK_BOX, pagesHeaderPane, CLASS_CSG_SITE_PAGES_CHECK_BOX, ENABLED);
         ohBuilder.buildCheckBox(CSG_SITE_PAGES_HWS_CHECK_BOX, pagesHeaderPane, CLASS_CSG_SITE_PAGES_CHECK_BOX  , ENABLED);
         
         //
         //Style Pane
         //
         VBox stylePane = ohBuilder.buildVBox(CSG_SITE_STYLE_PANE, sitePane, CLASS_CSG_PANE, ENABLED);
         HBox styleHeaderPane = ohBuilder.buildHBox(CSG_SITE_STYLE_HEADER_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox faviconPane = ohBuilder.buildHBox(CSG_SITE_FAVICON_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox navbarPane = ohBuilder.buildHBox(CSG_SITE_NAVBAR_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox leftFooterPane = ohBuilder.buildHBox(CSG_SITE_LEFTE_FOOTER_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox rightFooterPane = ohBuilder.buildHBox(CSG_SITE_RIGHT_FOOTER_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox styleSheetPane = ohBuilder.buildHBox(CSG_SITE_STYLE_SHEET_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         HBox notePane = ohBuilder.buildHBox(CSG_SITE_NOTE_PANE, stylePane, CLASS_CSG_BOX, ENABLED);
         
         ohBuilder.buildLabel(CSG_SITE_STYLE_HEADER_LABEL, styleHeaderPane, CLASS_CSG_HEADER_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_STYLE_SHEET_LABEL, styleSheetPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_NOTE_LABEL, notePane, CLASS_CSG_TEXT_LABEL, ENABLED);
         
         ohBuilder.buildTextButton(CSG_SITE_FAVICON_BUTTON, faviconPane, CLASS_CSG_IMPORT_IMAGE_BUTTON, ENABLED);
         ohBuilder.buildTextButton(CSG_SITE_NAVBAR_BUTTON, navbarPane, CLASS_CSG_IMPORT_IMAGE_BUTTON, ENABLED);
         ohBuilder.buildTextButton(CSG_SITE_LEFTE_FOOTER_BUTTON, leftFooterPane, CLASS_CSG_IMPORT_IMAGE_BUTTON, ENABLED);
         ohBuilder.buildTextButton(CSG_SITE_RIGHT_FOOTER_BUTTON, rightFooterPane, CLASS_CSG_IMPORT_IMAGE_BUTTON, ENABLED);
         
         ImageView faviconImage = ohBuilder.buildImageView(CSG_SITE_FAVICON_IMAGE_VIEW, faviconPane, CLASS_CSG_SITE_IMAGE_VIEW, ENABLED);
         ImageView navbarImage = ohBuilder.buildImageView(CSG_SITE_NAVBAR_IMAGE_VIEW, navbarPane, CLASS_CSG_SITE_IMAGE_VIEW, ENABLED);
         ImageView leftImage = ohBuilder.buildImageView(CSG_SITE_LEFTE_FOOTER_IMAGE_VIEW, leftFooterPane, CLASS_CSG_SITE_IMAGE_VIEW, ENABLED);
         ImageView rightImage = ohBuilder.buildImageView(CSG_SITE_RIGHT_FOOTER_IMAGE_VIEW, rightFooterPane, CLASS_CSG_SITE_IMAGE_VIEW, ENABLED);
        
         faviconImage.setImage(new Image("file:./images/favicon.png"));
         navbarImage.setImage(new Image("file:./images/navbar.png"));
         leftImage.setImage(new Image("file:./images/leftFooter.png"));
         rightImage.setImage(new Image("file:./images/rightFooter.png"));
         faviconImage.setFitWidth(200);
         faviconImage.setFitHeight(40);
         navbarImage.setFitWidth(200);
         navbarImage.setFitHeight(40);
         leftImage.setFitWidth(200);
         leftImage.setFitHeight(40);
         rightImage.setFitWidth(200);
         rightImage.setFitHeight(40);
         ohBuilder.buildComboBox(CSG_SITE_STYLE_SHEET_COMBO_BOX, CSG_SITE_STYLE_SHEET_OPTIONS, 
                        CSG_SITE_STYLE_SHEET_DEFAULT_OPTION, styleSheetPane, CLASS_CSG_COMBO_BOX, ENABLED);
         
         //
         //Instructor Pane
         //
         VBox instructorPane = ohBuilder.buildVBox(CSG_SITE_INSTRUCTOR_PANE, sitePane, CLASS_CSG_PANE, ENABLED);
         HBox instructorHeaderPane = ohBuilder.buildHBox(CSG_SITE_INSTRUCTOR_HEADER_PANE, instructorPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_INSTRUCTOR_HEADER_LABEL, instructorHeaderPane, CLASS_CSG_HEADER_LABEL, ENABLED);
         
         HBox namePane = ohBuilder.buildHBox(CSG_SITE_NAME_PANE, instructorPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_NAME_LABEL, namePane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildTextField(CSG_SITE_NAME_TEXT_FIELD, namePane, CLASS_CSG_TEXT_FIELD, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_ROOM_LABEL, namePane, CLASS_CSG_SITE_ROOM_TEXT_LABEL, ENABLED);
         ohBuilder.buildTextField(CSG_SITE_ROOM_TEXT_FIELD, namePane, CLASS_CSG_TEXT_FIELD, ENABLED);
         
         HBox emailPane = ohBuilder.buildHBox(CSG_SITE_EMAIL_PANE, instructorPane, CLASS_CSG_BOX, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_EMAIL_LABEL, emailPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildTextField(CSG_SITE_EMAIL_TEXT_FIELD, emailPane, CLASS_CSG_TEXT_FIELD, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_HOME_PAGE_LABEL, emailPane, CLASS_CSG_SITE_HOME_PAGE_TEXT_LABEL, ENABLED);
         ohBuilder.buildTextField(CSG_SITE_HOME_PAGE_TEXT_FIELD, emailPane, CLASS_CSG_TEXT_FIELD, ENABLED);
         
         HBox siteOHLabelPane = ohBuilder.buildHBox(CSG_SITE_OFFICE_HOURS_LABEL_PANE, instructorPane, CLASS_CSG_BOX, ENABLED);
         siteOHTextButton = ohBuilder.buildTextButton(CSG_SITE_OFFICE_HOURS_BUTTON, siteOHLabelPane, CLASS_CSG_BUTTON, ENABLED);
         ohBuilder.buildLabel(CSG_SITE_OFFICE_HOURS_LABEL, siteOHLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         siteOHTextButton.setText("-");
         HBox ohTextAreaPane = ohBuilder.buildHBox(CSG_SITE_OFFICE_HOURS_PANE, instructorPane, CLASS_CSG_BOX, ENABLED);
         siteTextArea = ohBuilder.buildTextArea(CSG_SITE_OFFICE_HOURS_TEXT_AREA, ohTextAreaPane, CLASS_CSG_TEXT_AREA , ENABLED);
         //*
         //*SYLLABUSS PANE
         //*
         ScrollPane syllabusScrollPane = new ScrollPane();
         
         VBox syllabusPane = ohBuilder.buildVBox(CSG_SYLLABUS_PANE, null, CLASS_CSG_MAIN_PANE, ENABLED);
         syllabusScrollPane.setContent(syllabusPane);
         syllabusPane.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
         syllabusPane.prefHeightProperty().bind(stage.widthProperty());
         
         VBox descriptionPane = ohBuilder.buildVBox(CSG_SYLLABUS_DESCRIPTION_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox topicsPane = ohBuilder.buildVBox(CSG_SYLLABUS_TOPICS_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox prerequisitesPane = ohBuilder.buildVBox(CSG_SYLLABUS_PREREQUISITES_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox outComesPane = ohBuilder.buildVBox(CSG_SYLLABUS_OUTCOMES_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox textbooksPane = ohBuilder.buildVBox(CSG_SYLLABUS_TEXTBOOKS_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox gcPane = ohBuilder.buildVBox(CSG_SYLLABUS_GRADED_COMPONENTS_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox gnPane = ohBuilder.buildVBox(CSG_SYLLABUS_GRADING_NOTE_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox adPane = ohBuilder.buildVBox(CSG_SYLLABUS_ACADEMIC_DISHONESTY_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         VBox saPane = ohBuilder.buildVBox(CSG_SYLLABUS_SPECIAL_ASSISTANCE_LABEL_PANE, syllabusPane, CLASS_CSG_PANE, ENABLED);
         
         HBox descriptionLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_DESCRIPTION_LABEL_PANE, descriptionPane, CLASS_CSG_BOX, ENABLED);
         HBox topicsLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_TOPICS_LABEL_PANE, topicsPane, CLASS_CSG_BOX, ENABLED);
         HBox prerequisitesLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_PREREQUISITES_LABEL_PANE, prerequisitesPane, CLASS_CSG_BOX, ENABLED);
         HBox outcomesLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_OUTCOMES_LABEL_PANE, outComesPane, CLASS_CSG_BOX, ENABLED);
         HBox textbooksLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_TEXTBOOKS_LABEL_PANE, textbooksPane, CLASS_CSG_BOX, ENABLED);
         HBox gcLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_GRADED_COMPONENTS_LABEL_PANE, gcPane, CLASS_CSG_BOX, ENABLED);
         HBox gnLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_GRADING_NOTE_LABEL_PANE, gnPane, CLASS_CSG_BOX, ENABLED);
         HBox adLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_ACADEMIC_DISHONESTY_LABEL_PANE, adPane, CLASS_CSG_BOX, ENABLED);
         HBox saLabelPane = ohBuilder.buildHBox(CSG_SYLLABUS_SPECIAL_ASSISTANCE_LABEL_PANE, saPane, CLASS_CSG_BOX, ENABLED);
         
         descriptionButton = ohBuilder.buildTextButton(CSG_SYLLABUS_DESCRIPTION_BUTTON, descriptionLabelPane, CLASS_CSG_BUTTON, ENABLED);
         topicsButton  = ohBuilder.buildTextButton(CSG_SYLLABUS_TOPICS_BUTTON, topicsLabelPane, CLASS_CSG_BUTTON, ENABLED);
         prerequisitesButton = ohBuilder.buildTextButton(CSG_SYLLABUS_PREREQUISITES_BUTTON, prerequisitesLabelPane, CLASS_CSG_BUTTON, ENABLED);
         outcomesButton = ohBuilder.buildTextButton(CSG_SYLLABUS_OUTCOMES_BUTTON, outcomesLabelPane, CLASS_CSG_BUTTON, ENABLED);
         textbooksButton = ohBuilder.buildTextButton(CSG_SYLLABUS_TEXTBOOKS_BUTTON, textbooksLabelPane, CLASS_CSG_BUTTON, ENABLED);        
         gcButton = ohBuilder.buildTextButton(CSG_SYLLABUS_GRADED_COMPONENTS_BUTTON, gcLabelPane, CLASS_CSG_BUTTON, ENABLED);       
         gnButton = ohBuilder.buildTextButton(CSG_SYLLABUS_GRADING_NOTE_BUTTON, gnLabelPane, CLASS_CSG_BUTTON, ENABLED);      
         adButton = ohBuilder.buildTextButton(CSG_SYLLABUS_ACADEMIC_DISHONESTY_BUTTON, adLabelPane, CLASS_CSG_BUTTON, ENABLED);      
         saButton = ohBuilder.buildTextButton(CSG_SYLLABUS_SPECIAL_ASSISTANCE_BUTTON,  saLabelPane, CLASS_CSG_BUTTON, ENABLED);     
         
         descriptionButton.setText("-");
         topicsButton.setText("+");
         prerequisitesButton.setText("+");
         outcomesButton.setText("+");
         textbooksButton.setText("+");         
         gcButton.setText("+");        
         gnButton.setText("+"); 
         adButton.setText("+");         
         saButton.setText("+");         
                         
         ohBuilder.buildLabel(CSG_SYLLABUS_DESCRIPTION_LABEL, descriptionLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_TOPICS_LABEL, topicsLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_PREREQUISITES_LABEL, prerequisitesLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_OUTCOMES_LABEL, outcomesLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_TEXTBOOKS_LABEL, textbooksLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_GRADED_COMPONENTS_LABEL, gcLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_GRADING_NOTE_LABEL, gnLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_ACADEMIC_DISHONESTY_LABEL, adLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         ohBuilder.buildLabel(CSG_SYLLABUS_SPECIAL_ASSISTANCE_LABEL, saLabelPane, CLASS_CSG_TEXT_LABEL, ENABLED);
         
         HBox descriptionTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_DESCRIPTION_TEXT_AREA_PANE, descriptionPane, CLASS_CSG_BOX, ENABLED);
         HBox topicsTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_TOPICS_TEXT_AREA_PANE, topicsPane, CLASS_CSG_BOX, ENABLED);
         HBox prerequisitesTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_PREREQUISITES_TEXT_AREA_PANE, prerequisitesPane, CLASS_CSG_BOX, ENABLED);
         HBox outcomesTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_OUTCOMES_TEXT_AREA_PANE, outComesPane, CLASS_CSG_BOX, ENABLED);
         HBox textbooksTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_TEXTBOOKS_TEXT_AREA_PANE, textbooksPane, CLASS_CSG_BOX, ENABLED);
         HBox gcTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_GRADED_COMPONENTS_TEXT_AREA_PANE, gcPane, CLASS_CSG_BOX, ENABLED);
         HBox gnTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_GRADING_NOTE_TEXT_AREA_PANE, gnPane, CLASS_CSG_BOX, ENABLED);
         HBox adTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_ACADEMIC_DISHONESTY_TEXT_AREA_PANE, adPane, CLASS_CSG_BOX, ENABLED);
         HBox saTextAreaPane = ohBuilder.buildHBox(CSG_SYLLABUS_SPECIAL_ASSISTANCE_TEXT_AREA_PANE, saPane, CLASS_CSG_BOX, ENABLED); 
        
         desTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_DESCRIPTION_TEXT_AREA, descriptionTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);
         topTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_TOPICS_TEXT_AREA, topicsTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);
         preTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_PREREQUISITES_TEXT_AREA, prerequisitesTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);        
         outTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_OUTCOMES_TEXT_AREA, outcomesTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);        
         texTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_TEXTBOOKS_TEXT_AREA, textbooksTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);        
         gcTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_GRADED_COMPONENTS_TEXT_AREA, gcTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);
         gnTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_GRADING_NOTE_TEXT_AREA, gnTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);
         adTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_ACADEMIC_DISHONESTY_TEXT_AREA, adTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);
         saTextArea = ohBuilder.buildTextArea(CSG_SYLLABUS_SPECIAL_ASSISTANCE_TEXT_AREA, saTextAreaPane, CLASS_CSG_TEXT_AREA, ENABLED);        
         
         preTextArea.setVisible(false);
        topTextArea.setVisible(false);
        outTextArea.setVisible(false);       
        texTextArea.setVisible(false);        
        gcTextArea.setVisible(false);
        gnTextArea.setVisible(false);
        adTextArea.setVisible(false);
        saTextArea.setVisible(false);
        preTextArea.setManaged(false);
        topTextArea.setManaged(false);
        outTextArea.setManaged(false);       
        texTextArea.setManaged(false);        
        gcTextArea.setManaged(false);
        gnTextArea.setManaged(false);
        adTextArea.setManaged(false);
        saTextArea.setManaged(false);        
        
        //*
        //*MEETING TIMES PANE
        //*
        ScrollPane mtsScrollPane = new ScrollPane();
        mtsTab.setContent(mtsScrollPane);
        VBox mtsPane = ohBuilder.buildVBox(CSG_MEETING_TIMES_PANE, null, CLASS_CSG_MAIN_PANE, ENABLED);
        mtsScrollPane.setContent(mtsPane);
        mtsPane.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        mtsPane.prefHeightProperty().bind(stage.widthProperty());
        //
        //Lectures Pane
        //
        VBox lecturesPane = ohBuilder.buildVBox(CSG_MEETING_TIMES_LECTURES_PANE, mtsPane, CLASS_CSG_PANE, ENABLED);
        HBox lecturesHeaderPane = ohBuilder.buildHBox(CSG_MEETING_TIMES_LECTURES_HEADER_PANE, lecturesPane, CLASS_CSG_BOX , ENABLED);
        Button lecAddBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_LECTURES_ADD_BUTTON, lecturesHeaderPane, CLASS_CSG_BUTTON , ENABLED);
        Button lecRemoveBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_LECTURES_REMOVE_BUTTON, lecturesHeaderPane, CLASS_CSG_BUTTON , ENABLED);
        ohBuilder.buildLabel(CSG_MEETING_TIMES_LECTURES_HEADER_LABEL, lecturesHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED); 
        TableView<MeetingTimes> lecturesTable = ohBuilder.buildTableView(CSG_MEETING_TIMES_LECTURES_TABLE_VIEW, lecturesPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        lecturesTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn lecSectionColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LECTURES_SECTION_TABLE_COLUMN, lecturesTable, CLASS_CSG_COLUMN);
        TableColumn lecDaysColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LECTURES_DAYS_TABLE_COLUMN, lecturesTable, CLASS_CSG_COLUMN);
        TableColumn lecTimeColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LECTURES_TIME_TABLE_COLUMN, lecturesTable, CLASS_CSG_COLUMN);
        TableColumn lecRoomColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LECTURES_ROOM_TABLE_COLUMN, lecturesTable, CLASS_CSG_COLUMN);
        lecAddBt.setText("+");
        lecRemoveBt.setText("-");
        lecSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        lecDaysColumn.setCellValueFactory(new PropertyValueFactory<String, String>("days"));
        lecTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("time"));
        lecRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        

        lecSectionColumn.prefWidthProperty().bind(lecturesTable.widthProperty().multiply(1.0 / 4.0));
        lecDaysColumn.prefWidthProperty().bind(lecturesTable.widthProperty().multiply(1.0 / 4.0));
        lecTimeColumn.prefWidthProperty().bind(lecturesTable.widthProperty().multiply(1.0 / 4.0));
        lecRoomColumn.prefWidthProperty().bind(lecturesTable.widthProperty().multiply(1.0 / 4.0));
        lecturesTable.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));

        //
        //Recitation Pane
        //
        VBox recitationPane = ohBuilder.buildVBox(CSG_MEETING_TIMES_RECITATIONIS_PANE, mtsPane, CLASS_CSG_PANE, ENABLED);
        HBox recitationHeaderPane = ohBuilder.buildHBox(CSG_MEETING_TIMES_RECITATIONIS_HEADER_PANE, recitationPane, CLASS_CSG_BOX, ENABLED);
        Button recAddBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_RECITATIONIS_ADD_BUTTON, recitationHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        Button recRemoveBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_RECITATIONIS_REMOVE_BUTTON, recitationHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        ohBuilder.buildLabel(CSG_MEETING_TIMES_RECITATIONIS_HEADER_LABEL, recitationHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        TableView<MeetingTimes> recitationTable = ohBuilder.buildTableView(CSG_MEETING_TIMES_RECITATIONIS_TABLE_VIEW, recitationPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        TableColumn recSectionColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_RECITATIONIS_SECTION_TABLE_COLUMN, recitationTable, CLASS_CSG_COLUMN);
        TableColumn recDaysTimeColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_RECITATIONIS_DAYS_AND_TIME_TABLE_COLUMN, recitationTable, CLASS_CSG_COLUMN);
        TableColumn recRoomColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_RECITATIONIS_ROOM_TABLE_COLUMN, recitationTable, CLASS_CSG_COLUMN);
        TableColumn recTA1Column = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_RECITATIONIS_TA1_TABLE_COLUMN, recitationTable, CLASS_CSG_COLUMN);
        TableColumn recTA2Column = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_RECITATIONIS_TA2_TABLE_COLUMN, recitationTable, CLASS_CSG_COLUMN);
        
        recAddBt.setText("+");
        recRemoveBt.setText("-");
        recSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        recDaysTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("daysAndTime"));
        recRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        recTA1Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        recTA2Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        recSectionColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1.0 / 5.0));
        recDaysTimeColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(2.0 / 5.0));
        recRoomColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1.0 / 5.0));
        recTA1Column.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1.0 / 5.0));
        recTA2Column.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1.0 / 5.0));

        //
        //Lab Pane
        //
        VBox labPane = ohBuilder.buildVBox(CSG_MEETING_TIMES_LABS_PANE, mtsPane, CLASS_CSG_PANE, ENABLED); 
        HBox labHeaderPane = ohBuilder.buildHBox(CSG_MEETING_TIMES_LABS_HEADER_PANE, labPane, CLASS_CSG_BOX, ENABLED);
        Button labAddBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_LABS_ADD_BUTTON, labHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        Button labRemoveBt = ohBuilder.buildTextButton(CSG_MEETING_TIMES_LABS_REMOVE_BUTTON, labHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        ohBuilder.buildLabel(CSG_MEETING_TIMES_LABS_HEADER_LABEL, labHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        TableView<MeetingTimes> labTable = ohBuilder.buildTableView(CSG_MEETING_TIMES_LABS_TABLE_VIEW, labPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        TableColumn labSectionColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LABS_SECTION_TABLE_COLUMN, labTable, CLASS_CSG_COLUMN);
        TableColumn labDaysTimeColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LABS_DAYS_AND_TIME_TABLE_COLUMN, labTable, CLASS_CSG_COLUMN);
        TableColumn labRoomColumn = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LABS_ROOM_TABLE_COLUMN, labTable, CLASS_CSG_COLUMN);
        TableColumn labTA1Column = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LABS_TA1_TABLE_COLUMN, labTable, CLASS_CSG_COLUMN);
        TableColumn labTA2Column = ohBuilder.buildTableColumn(CSG_MEETING_TIMES_LABS_TA2_TABLE_COLUMN, labTable, CLASS_CSG_COLUMN);
        labAddBt.setText("+");
        labRemoveBt.setText("-");
        labSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        labDaysTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("daysAndTime"));
        labRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        labTA1Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        labTA2Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        labSectionColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labDaysTimeColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(2.0 / 5.0));
        labRoomColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labTA1Column.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labTA2Column.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        
        
        
        //*
        //*OFFICE HOURS PANE
        //*
        
        VBox ohPane = ohBuilder.buildVBox(CSG_OFFICE_HOURS_PANE, null, CLASS_CSG_MAIN_PANE, ENABLED);
        
        ohPane.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        ohPane.prefHeightProperty().bind(stage.widthProperty());
        //
        //Top Pane
        //
        VBox topPane = ohBuilder.buildVBox(CSG_OFFICE_HOURS_TOP_PANE, ohPane, CLASS_CSG_PANE, ENABLED);
        HBox tasHeaderPane =ohBuilder.buildHBox(CSG_OFFICE_HOURS_TAS_HEADER_PANE, topPane, CLASS_CSG_COMBO_BOX, ENABLED);
        Button removeTA = ohBuilder.buildTextButton(CSG_OFFICE_HOURS_REMOVE_TA_BUTTON, tasHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        removeTA.setText("-");
        ohBuilder.buildLabel(CSG_OFFICE_HOURS_TAS_HEADER_LABEL, tasHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        
        HBox radioButtonPane = ohBuilder.buildHBox(CSG_OFFICE_HOURS_RADIO_BUTTON_PANE, tasHeaderPane, CLASS_CSG_COMBO_BOX, ENABLED);
        ToggleGroup csgToggleGroup = new ToggleGroup();
        ohBuilder.buildRadioButton(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON, radioButtonPane, CLASS_CSG_OFFICE_HOURS_RADIO_BUTTON , ENABLED, csgToggleGroup, ENABLED);
        ohBuilder.buildRadioButton(CSG_OFFICE_HOURS_GRADUATE_RADIO_BUTTON, radioButtonPane, CLASS_CSG_OFFICE_HOURS_RADIO_BUTTON , ENABLED, csgToggleGroup, false);
        ohBuilder.buildRadioButton(CSG_OFFICE_HOURS_UNDERGRADUATE_RADIO_BUTTON, radioButtonPane, CLASS_CSG_OFFICE_HOURS_RADIO_BUTTON , ENABLED, csgToggleGroup, false);
        
        TableView<TeachingAssistantPrototype> csgTaTable = ohBuilder.buildTableView(CSG_OFFICE_HOURS_TAS_TABLE_VIEW, topPane, CLASS_CSG_TABLE_VIEW , ENABLED);
        TableColumn csgOHNameColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_NAME_TABLE_COLUMN, csgTaTable, CLASS_CSG_COLUMN);
        TableColumn csgOHEmailColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_EMAIL_TABLE_COLUMN, csgTaTable, CLASS_CSG_COLUMN);
        TableColumn csgOHTimeSlotsColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_SLOTS_TABLE_COLUMN, csgTaTable, CLASS_CSG_COLUMN);
        TableColumn csgOHTypeColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_TYPE_TABLE_COLUMN, csgTaTable, CLASS_CSG_COLUMN);
        csgOHNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        csgOHEmailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        csgOHTimeSlotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("timeSlots"));
        csgOHTypeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        csgOHNameColumn.prefWidthProperty().bind(csgTaTable.widthProperty().multiply(1.0 / 4.0));
        csgOHEmailColumn.prefWidthProperty().bind(csgTaTable.widthProperty().multiply(1.0 / 4.0));
        csgOHTimeSlotsColumn.prefWidthProperty().bind(csgTaTable.widthProperty().multiply(1.0 / 4.0));
        csgOHTypeColumn.prefWidthProperty().bind(csgTaTable.widthProperty().multiply(1.0 / 4.0));
        csgTaTable.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        
        GridPane addTaPane = ohBuilder.buildGridPane(CSG_OFFICE_HOURS_ADD_TA_PANE, topPane, CLASS_CSG_OFFICE_HOURS_ADD_TA_BUTTON_GRID_PANE, ENABLED);
        ohBuilder.buildTextField(CSG_OFFICE_HOURS_NAME_TEXT_FIELD, addTaPane,0, 0, 1, 1, CLASS_CSG_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD, addTaPane, 1, 0, 1, 1, CLASS_CSG_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(CSG_OFFICE_HOURS_ADD_TA_BUTTON, addTaPane, 2, 0, 1, 1, CLASS_CSG_TEXT_FIELD, ENABLED);
        //
        //Bottom Pane
        //
        VBox bottomPane = ohBuilder.buildVBox(CSG_OFFICE_HOURS_BOTTOM_PANE, ohPane, CLASS_CSG_PANE, ENABLED); 
        HBox ohHeaderPane = ohBuilder.buildHBox(CSG_OFFICE_HOURS_HEADER_PANE, bottomPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_OFFICE_HOURS_HEADER_LABEL, ohHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        HBox ohTimePane = ohBuilder.buildHBox(CSG_OFFICE_HOURS_TIME_PANE, ohHeaderPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_OFFICE_HOURS_START_TIME_LABEL, ohTimePane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ComboBox startTimeBox = ohBuilder.buildComboBox(CSG_OFFICE_HOURS_START_TIME_COMBO_BOX, CSG_OFFICE_HOURS_START_TIME_OPTIONS, EMPTY_TEXT, ohTimePane, CLASS_CSG_COMBO_BOX, ENABLED);
        ohBuilder.buildLabel(CSG_OFFICE_HOURS_END_TIME_LABEL, ohTimePane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ComboBox endTimeBox =ohBuilder.buildComboBox(CSG_OFFICE_HOURS_END_TIME_COMBO_BOX, CSG_OFFICE_HOURS_END_TIME_OPTIONS, EMPTY_TEXT, ohTimePane, CLASS_CSG_COMBO_BOX, ENABLED);
        startTimeBox.getSelectionModel().selectFirst();
        endTimeBox.getSelectionModel().selectLast();
        TableView csgOHTable = ohBuilder.buildTableView(CSG_OFFICE_HOURS_TABLE_VIEW, bottomPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        TableColumn csgStartTimeColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_START_TIME_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgEndTimeColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_END_TIME_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgMondayColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_MONDAY_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgTuesdayColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_TUESDAY_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgWednesdayColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_WEDNESDAY_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgThursdayColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_THURSDAY_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
        TableColumn csgFridayColumn = ohBuilder.buildTableColumn(CSG_OFFICE_HOURS_FRIDAY_TABLE_COLUMN, csgOHTable, CLASS_CSG_COLUMN);
                
        csgStartTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        csgEndTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        csgMondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        csgTuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        csgWednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        csgThursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        csgFridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
                
        csgStartTimeColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgEndTimeColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgMondayColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgTuesdayColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgWednesdayColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgThursdayColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgFridayColumn.prefWidthProperty().bind(csgOHTable.widthProperty().multiply(1.0 / 7.0));
        csgOHTable.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        //*
        //*SCHEDULE PANE
        //*
        ScrollPane scheduleScrollPane = new ScrollPane();
        VBox schedulePane = ohBuilder.buildVBox(notePane, null, CLASS_CSG_MAIN_PANE, ENABLED);
        scheduleScrollPane.setContent(schedulePane);
        
        schedulePane.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        schedulePane.prefHeightProperty().bind(stage.widthProperty());
        //
        //Calendar Pane
        //
        VBox calendarPane = ohBuilder.buildVBox(CSG_SCHEDULE_CALENDAR_PANE, schedulePane, CLASS_CSG_PANE, ENABLED);
        HBox calendarHeaderPane = ohBuilder.buildHBox(CSG_SCHEDULE_CALENDAR_HEADER_PANE, calendarPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_CALENDAR_HEADER_LABEL, calendarHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        
        HBox calendarTimePane = ohBuilder.buildHBox(CSG_SCHEDULE_CALENDAR_TIME_PANE, calendarPane, CLASS_CSG_BOX, ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_CALENDAR_STARTING_MONDAY_LABEL, calendarTimePane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ohBuilder.buildDatePicker(CSG_SCHEDULE_CALENDAR_STARTING_MONDAY_DATE_PICKER, calendarTimePane, CLASS_CSG_SCHEDULE_DATE_PICKER, ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_CALENDAR_ENDTING_FRIDAY_LABEL, calendarTimePane, CLASS_CSG_TEXT_LABEL, ENABLED);
       ohBuilder.buildDatePicker(CSG_SCHEDULE_CALENDAR_ENDTING_FRIDAY_DATE_PICKER, calendarTimePane, CLASS_CSG_SCHEDULE_DATE_PICKER, ENABLED);
        //
        //Schedule Items Pane
        //
        VBox scheduleItemsPane = ohBuilder.buildVBox(CSG_SCHEDULE_SCHEDULE_ITEMS_PANE, schedulePane, CLASS_CSG_PANE, ENABLED);
        
        HBox scheduleItemsHeaderPane = ohBuilder.buildHBox(CSG_SCHEDULE_SCHEDULE_ITEMS_HEADER_PANE, scheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        Button RemoveShceduleItemsBt = ohBuilder.buildTextButton(CSG_SCHEDULE_SCHEDULE_ITEMS_REMOVE_BUTTON, scheduleItemsHeaderPane, CLASS_CSG_BUTTON, ENABLED);
        RemoveShceduleItemsBt.setText("-");
        ohBuilder.buildLabel(CSG_SCHEDULE_SCHEDULE_ITEMS_HEADER_LABEL, scheduleItemsHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        
        TableView scheduleItemsTable = ohBuilder.buildTableView(CSG_SCHEDULE_SCHEDULE_ITEMS_TABLE_VIEW, scheduleItemsPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        TableColumn csgSITypeColumn = ohBuilder.buildTableColumn(CSG_SCHEDULE_SCHEDULE_TYPE_TABLE_COLUMN, scheduleItemsTable, CLASS_CSG_COLUMN);
        TableColumn csgSIDateColumn = ohBuilder.buildTableColumn(CSG_SCHEDULE_SCHEDULE_DATE_TABLE_COLUMN, scheduleItemsTable, CLASS_CSG_COLUMN);
        TableColumn csgSITitleColumn = ohBuilder.buildTableColumn(CSG_SCHEDULE_SCHEDULE_TITLE_TABLE_COLUMN, scheduleItemsTable, CLASS_CSG_COLUMN);
        TableColumn csgSITopicColumn = ohBuilder.buildTableColumn(CSG_SCHEDULE_SCHEDULE_TOPIC_TABLE_COLUMN, scheduleItemsTable, CLASS_CSG_COLUMN);
                
        csgSITypeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        csgSIDateColumn.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        csgSITitleColumn.setCellValueFactory(new PropertyValueFactory<String, String>("title"));
        csgSITopicColumn.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
                
        csgSITypeColumn.prefWidthProperty().bind(scheduleItemsTable.widthProperty().multiply(1.0 / 5.0));
        csgSIDateColumn.prefWidthProperty().bind(scheduleItemsTable.widthProperty().multiply(1.0 / 5.0));
        csgSITitleColumn.prefWidthProperty().bind(scheduleItemsTable.widthProperty().multiply(1.0 / 5.0));
        csgSITopicColumn.prefWidthProperty().bind(scheduleItemsTable.widthProperty().multiply(2.0 / 5.0));
        scheduleItemsTable.prefWidthProperty().bind(stage.widthProperty().multiply(19.0 / 20.0));
        //
        //Add Schedule Item Pane
        //
        VBox addScheduleItemsPane = ohBuilder.buildVBox(CSG_SCHEDULE_ADD_SCHEDULE_ITEMS_PANE, schedulePane, CLASS_CSG_PANE, ENABLED);
        HBox addScheduleItemsHeaderPane = ohBuilder.buildHBox(CSG_SCHEDULE_ADD_SCHEDULE_HEADER_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_ADD_SCHEDULE_HEADER_LABEL, addScheduleItemsHeaderPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        
        HBox siTypePane = ohBuilder.buildHBox(CSG_SCHEDULE_TYPE_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_TYPE_LABEL, siTypePane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ohBuilder.buildComboBox(CSG_SCHEDULE_TYPE_COMBO_BOX, CSG_SCHEDULE_TYPE_OPTIONS, CSG_SCHEDULE_TYPE_DEFAULT_OPTION, siTypePane, CLASS_CSG_COMBO_BOX, ENABLED);
        
        HBox siDatePane = ohBuilder.buildHBox(CSG_SCHEDULE_DATE_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_DATE_LABEL, siDatePane, CLASS_CSG_TEXT_LABEL, ENABLED);
       ohBuilder.buildDatePicker(CSG_SCHEDULE_DATE_DATE_PICKER, siDatePane, CLASS_CSG_SCHEDULE_DATE_PICKER, ENABLED);
        
        HBox siTitlePane = ohBuilder.buildHBox(CSG_SCHEDULE_TITLE_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_TITLE_LABEL, siTitlePane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ohBuilder.buildTextField(CSG_SCHEDULE_TITLE_TEXT_FIELD, siTitlePane, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        HBox siTopicPane = ohBuilder.buildHBox(CSG_SCHEDULE_TOPIC_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_TOPIC_LABEL, siTopicPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ohBuilder.buildTextField(CSG_SCHEDULE_TOPIC_TEXT_FIELD, siTopicPane, CLASS_CSG_TEXT_FIELD, ENABLED);
       
        HBox siLinkPane = ohBuilder.buildHBox(CSG_SCHEDULE_LINK_PANE, addScheduleItemsPane, CLASS_CSG_BOX , ENABLED);
        ohBuilder.buildLabel(CSG_SCHEDULE_LINK_LABEL, siLinkPane, CLASS_CSG_TEXT_LABEL, ENABLED);
        ohBuilder.buildTextField(CSG_SCHEDULE_LINK_TEXT_FIELD, siLinkPane, CLASS_CSG_TEXT_FIELD, ENABLED);
       
        GridPane siAddBtPane = ohBuilder.buildGridPane(CSG_SCHEDULE_ADD_BUTTON_PANE, addScheduleItemsPane, 
                CLASS_CSG_SCHEDULE_FUNCTION_BUTTON_GRID_PANE, ENABLED);
          ohBuilder.buildTextButton(CSG_SCHEDULE_ADD_BUTTON, siAddBtPane, 1, 1, 1, 1, CLASS_CSG_FUNCTION_BUTTON, ENABLED);
          ohBuilder.buildTextButton(CSG_SCHEDULE_CLEAR_BUTTON, siAddBtPane, 2, 1, 1, 1, CLASS_CSG_FUNCTION_BUTTON, ENABLED);
        
        siteTab.setContent(siteScrollPane);
        syllabusTab.setContent(syllabusScrollPane);
        mtsTab.setContent(mtsScrollPane);
        ohTab.setContent(ohPane);
        scheduleTab.setContent(scheduleScrollPane);
        //Put into workSpace
        VBox.setVgrow(lecturesTable, Priority.ALWAYS);
        workspace = new BorderPane();
        ((BorderPane)workspace).setCenter(tabPane);
         
    }

    private void setupOfficeHoursColumn(Object columnId, TableView tableView, String styleClass, String columnDataProperty) {
        AppNodesBuilder builder = app.getGUIModule().getNodesBuilder();
        TableColumn<TeachingAssistantPrototype, String> column = builder.buildTableColumn(columnId, tableView, styleClass);
        column.setCellValueFactory(new PropertyValueFactory<TeachingAssistantPrototype, String>(columnDataProperty));
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(1.0 / 7.0));
        column.setCellFactory(col -> {
            return new TableCell<TeachingAssistantPrototype, String>() {
                @Override
                protected void updateItem(String text, boolean empty) {
                    super.updateItem(text, empty);
                    if (text == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        // CHECK TO SEE IF text CONTAINS THE NAME OF
                        // THE CURRENTLY SELECTED TA
                        setText(text);
                        TableView<TeachingAssistantPrototype> tasTableView = (TableView) app.getGUIModule().getGUINode(OH_TAS_TABLE_VIEW);
                        TeachingAssistantPrototype selectedTA = tasTableView.getSelectionModel().getSelectedItem();
                        if (selectedTA == null) {
                            setStyle("");
                        } else if (text.contains(selectedTA.getName())) {
                            setStyle("-fx-background-color: yellow");
                        } else {
                            setStyle("");
                        }
                    }
                }
            };
        });
    }

    public void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((CourseSiteGeneratorApp) app);
        CourseSiteGeneratorController csgController = new CourseSiteGeneratorController((CourseSiteGeneratorApp) app);
        AppGUIModule gui = app.getGUIModule();
        
        siteOHTextButton.setOnAction(e -> {
            if(siteOHTextButton.getText().equals("-")) {
                siteTextArea.setVisible(false);
                siteTextArea.setManaged(false);
                siteOHTextButton.setText("+");
            }
            else {
                siteTextArea.setVisible(true);
                siteTextArea.setManaged(true);
                siteOHTextButton.setText("-");
            }
        });
        descriptionButton.setOnAction(e -> {
            if(descriptionButton.getText().equals("-")) {
                desTextArea.setVisible(false);
                desTextArea.setManaged(false);
                descriptionButton.setText("+");
            }
            else {
                desTextArea.setVisible(true);
                desTextArea.setManaged(true);
                descriptionButton.setText("-");
            }
        });
        topicsButton.setOnAction(e -> {
            if(topicsButton.getText().equals("-")) {
                topTextArea.setVisible(false);
                topTextArea.setManaged(false);
                topicsButton.setText("+");
            }
            else {
                topTextArea.setVisible(true);
                topTextArea.setManaged(true);
                topicsButton.setText("-");
            }
        });
        
         prerequisitesButton.setOnAction(e -> {
            if(prerequisitesButton.getText().equals("-")) {
                preTextArea.setVisible(false);
                preTextArea.setManaged(false);
                prerequisitesButton.setText("+");
            }
            else {
                preTextArea.setVisible(true);
                preTextArea.setManaged(true);
                prerequisitesButton.setText("-");
            }
        });
         outcomesButton.setOnAction(e -> {
            if(outcomesButton.getText().equals("-")) {
                outTextArea.setVisible(false);
                outTextArea.setManaged(false);
                outcomesButton.setText("+");
            }
            else {
                outTextArea.setVisible(true);
                outTextArea.setManaged(true);
                outcomesButton.setText("-");
            }
        }); 
         textbooksButton.setOnAction(e -> {
            if(textbooksButton.getText().equals("-")) {
                texTextArea.setVisible(false);
                texTextArea.setManaged(false);
                textbooksButton.setText("+");
            }
            else {
                texTextArea.setVisible(true);
                texTextArea.setManaged(true);
                textbooksButton.setText("-");
            }
        }); 
         gcButton.setOnAction(e -> {
            if(gcButton.getText().equals("-")) {
                gcTextArea.setVisible(false);
                gcTextArea.setManaged(false);
                gcButton.setText("+");
            }
            else {
                gcTextArea.setVisible(true);
                gcTextArea.setManaged(true);
                gcButton.setText("-");
            }
        });
         gnButton.setOnAction(e -> {
            if(gnButton.getText().equals("-")) {
                gnTextArea.setVisible(false);
                gnTextArea.setManaged(false);
                gnButton.setText("+");
            }
            else {
                gnTextArea.setVisible(true);
                gnTextArea.setManaged(true);
                gnButton.setText("-");
            }
        });
         adButton.setOnAction(e -> {
            if(adButton.getText().equals("-")) {
                adTextArea.setVisible(false);
                adTextArea.setManaged(false);
                adButton.setText("+");
            }
            else {
                adTextArea.setVisible(true);
                adTextArea.setManaged(true);
                adButton.setText("-");
            }
        });
         saButton.setOnAction(e -> {
            if(saButton.getText().equals("-")) {
                saTextArea.setVisible(false);
                saTextArea.setManaged(false);
                saButton.setText("+");
            }
            else {
                saTextArea.setVisible(true);
                saTextArea.setManaged(true);
                saButton.setText("-");
            }
        }); 
         ((Button) gui.getGUINode(CSG_SITE_FAVICON_BUTTON)).setOnAction(e -> {
            csgController.processFaviconButton();
        });
          ((Button) gui.getGUINode(CSG_SITE_NAVBAR_BUTTON)).setOnAction(e -> {
            csgController.processNavbarButton();
        });
           ((Button) gui.getGUINode(CSG_SITE_LEFTE_FOOTER_BUTTON)).setOnAction(e -> {
            csgController.processLefterFooterButton();
        });
            ((Button) gui.getGUINode(CSG_SITE_RIGHT_FOOTER_BUTTON)).setOnAction(e -> {
            csgController.processRightFooterButton();
        });
         /*********************************************/
        TextField csgNameTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_NAME_TEXT_FIELD));
        TextField csgEmailTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD));

        csgNameTextField.textProperty().addListener(e -> {
            csgController.processTypeTA();
        });
        csgEmailTextField.textProperty().addListener(e -> {
            csgController.processTypeTA();
        });

        // FIRE THE ADD EVENT ACTION
        csgNameTextField.setOnAction(e -> {
            csgController.processAddTA();
        });
        csgEmailTextField.setOnAction(e -> {
            csgController.processAddTA();
        });
        ((Button) gui.getGUINode(CSG_OFFICE_HOURS_ADD_TA_BUTTON)).setOnAction(e -> {
            csgController.processAddTA();
        });
        ((Button) gui.getGUINode(CSG_OFFICE_HOURS_REMOVE_TA_BUTTON)).setOnAction(e -> {
            csgController.processRemoveTA();
        });
         TableView csgOfficeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        csgOfficeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        csgOfficeHoursTableView.setOnMouseClicked(e -> {
            csgController.processToggleOfficeHours();
        });
         TableView csgTasTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        for (int i = 0; i < csgOfficeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) csgOfficeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < csgTasTableView.getColumns().size(); i++) {
            ((TableColumn) csgTasTableView.getColumns().get(i)).setSortable(false);
        }
        csgTasTableView.getSelectionModel().selectedItemProperty().addListener(e -> {
            csgController.processTypeTA();
        });
        csgTasTableView.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                csgController.processEditTA();
            }
            csgController.processSelectTA();
        });
        
        RadioButton csgAllRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON);
        csgAllRadio.setOnAction(e -> {
            csgController.processSelectAllTAs();
        });
        RadioButton csgGradRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_GRADUATE_RADIO_BUTTON);
        csgGradRadio.setOnAction(e -> {
            csgController.processSelectGradTAs();
        });
        RadioButton csgUndergradRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_UNDERGRADUATE_RADIO_BUTTON);
        csgUndergradRadio.setOnAction(e -> {
            csgController.processSelectUndergradTAs();
        });
        ComboBox startTimeBox = ((ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBO_BOX));
        ComboBox endTimeBox = ((ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBO_BOX));
//        ((ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBO_BOX)).setOnAction(e -> {
//            csgController.processOHSelectTime(); 
//            csgController.processTypeTA();
//        });
//        ((ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBO_BOX)).setOnAction(e -> {
//            csgController.processOHSelectTime(); 
//            csgController.processTypeTA();
//        });
       startTimeBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal)->{
           if (newVal != null && oldVal != null) {
                if(!newVal.equals(oldVal)){
                    csgController.processOHSelectTime((String)oldVal, (String)newVal, (String)endTimeBox.getValue(), (String)endTimeBox.getValue()); 
                    csgController.processTypeTA();
                }
           }
        }); 
       endTimeBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal)->{
           if (newVal != null && oldVal != null) {
            if(!newVal.equals(oldVal)){
                 csgController.processOHSelectTime((String)startTimeBox.getValue(), (String)startTimeBox.getValue(), (String)oldVal, (String)newVal); 
                 csgController.processTypeTA();
             }
           }
        }); 

        /*****************************************************************/
        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));

        nameTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });

        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });

        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleOfficeHours();
        });

        // DON'T LET ANYONE SORT THE TABLES
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn) tasTableView.getColumns().get(i)).setSortable(false);
        }

        tasTableView.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                controller.processEditTA();
            }
            controller.processSelectTA();
        });

        RadioButton allRadio = (RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON);
        allRadio.setOnAction(e -> {
            controller.processSelectAllTAs();
        });
        RadioButton gradRadio = (RadioButton) gui.getGUINode(OH_GRAD_RADIO_BUTTON);
        gradRadio.setOnAction(e -> {
            controller.processSelectGradTAs();
        });
        RadioButton undergradRadio = (RadioButton) gui.getGUINode(OH_UNDERGRAD_RADIO_BUTTON);
        undergradRadio.setOnAction(e -> {
            controller.processSelectUndergradTAs();
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((CourseSiteGeneratorApp) app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}
