package csg.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_ADD_TA_BUTTON;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_END_TIME_COMBO_BOX;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_NAME_TEXT_FIELD;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_REMOVE_TA_BUTTON;
import static csg.CourseSiteGeneratorPropertyType.CSG_OFFICE_HOURS_START_TIME_COMBO_BOX;

import csg.data.OfficeHoursData;
import static csg.workspace.style.CSGStyle.CLASS_OH_TEXT_FIELD;
import static csg.workspace.style.CSGStyle.CLASS_OH_TEXT_FIELD_ERROR;
import javafx.scene.control.ComboBox;


public class OfficeHoursFoolproofDesign implements FoolproofDesign {

    CourseSiteGeneratorApp app;
//    private final String[] comboBox
//                = {"9:00am", "9:30am", "10:00am", "10:30am", "11:00am", "11:30am", "12:00pm", "12:30pm", "1:30pm", "2:00pm", "2:30pm",
//                     "3:00pm", "3:30pm", "4:00pm", "4:30pm", "5:00pm", "5:30pm", "6:00pm", "6:30pm", "7:00pm", "7:30pm", "8:00pm", "8:30pm",
//                     "9:00pm"}; 
    private final String[] startTimeComboBox
                = {"9:00am", "9:30am", "10:00am", "10:30am", "11:00am", "11:30am", "12:00pm", "12:30pm", "1:00pm", "1:30pm", "2:00pm", "2:30pm",
                     "3:00pm", "3:30pm", "4:00pm", "4:30pm", "5:00pm", "5:30pm", "6:00pm", "6:30pm", "7:00pm", "7:30pm", "8:00pm", "8:30pm",
                    }; 
    private final String[] endTimeComboBox 
                = {"9:30am", "10:00am", "10:30am", "11:00am", "11:30am", "12:00pm", "12:30pm", "1:00pm","1:30pm", "2:00pm", "2:30pm",
                     "3:00pm", "3:30pm", "4:00pm", "4:30pm", "5:00pm", "5:30pm", "6:00pm", "6:30pm", "7:00pm", "7:30pm", "8:00pm", "8:30pm",
                    "9:00pm"};            
    public OfficeHoursFoolproofDesign(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        updateAddTAFoolproofDesign();
        updateEditTAFoolproofDesign();
        updateRemoveTAFoolproofDesign();
        updateSelecteTimeRangeFoolProofDesign();
    }

    private void updateAddTAFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        
        // FOOLPROOF DESIGN STUFF FOR ADD TA BUTTON
        TextField nameTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD));
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = (Button) gui.getGUINode(CSG_OFFICE_HOURS_ADD_TA_BUTTON);

        // FIRST, IF NO TYPE IS SELECTED WE'LL JUST DISABLE
        // THE CONTROLS AND BE DONE WITH IT
        boolean isTypeSelected = data.isTATypeSelected();
        if (!isTypeSelected) {
            nameTextField.setDisable(true);
            emailTextField.setDisable(true);
            addTAButton.setDisable(true);
            return;
        } // A TYPE IS SELECTED SO WE'LL CONTINUE
        else {
            nameTextField.setDisable(false);
            emailTextField.setDisable(false);
            addTAButton.setDisable(false);
        }

        // NOW, IS THE USER-ENTERED DATA GOOD?
        boolean isLegalNewTA = data.isLegalNewTA(name, email);

        // ENABLE/DISABLE THE CONTROLS APPROPRIATELY
        addTAButton.setDisable(!isLegalNewTA);
        if (isLegalNewTA) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
        } else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
        }

        // UPDATE THE CONTROL TEXT DISPLAY APPROPRIATELY
        boolean isLegalNewName = data.isLegalNewName(name);
        boolean isLegalNewEmail = data.isLegalNewEmail(email);
        foolproofTextField(nameTextField, isLegalNewName);
        foolproofTextField(emailTextField, isLegalNewEmail);
    }
    
    private void updateEditTAFoolproofDesign() {
        
    }
    
    public void foolproofTextField(TextField textField, boolean hasLegalData) {
        if (hasLegalData) {
            textField.getStyleClass().remove(CLASS_OH_TEXT_FIELD_ERROR);
            if (!textField.getStyleClass().contains(CLASS_OH_TEXT_FIELD)) {
                textField.getStyleClass().add(CLASS_OH_TEXT_FIELD);
            }
        } else {
            textField.getStyleClass().remove(CLASS_OH_TEXT_FIELD);
            if (!textField.getStyleClass().contains(CLASS_OH_TEXT_FIELD_ERROR)) {
                textField.getStyleClass().add(CLASS_OH_TEXT_FIELD_ERROR);
            }
        }
    }
    public void updateRemoveTAFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        Button removeTABt = ((Button) gui.getGUINode(CSG_OFFICE_HOURS_REMOVE_TA_BUTTON));
        if(data.isTASelected()) {
            removeTABt.setDisable(false);
        }
        else {
            removeTABt.setDisable(true);
        }
    }
    public void updateSelecteTimeRangeFoolProofDesign() {
        AppGUIModule gui = app.getGUIModule();
        ComboBox startTimeBox = (ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBO_BOX);
        ComboBox endTimeBox = (ComboBox)gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBO_BOX);
        String startTime = (String)startTimeBox.getValue();
        String endTime = (String)endTimeBox.getValue();
        int startTimeIndex = 0;
        int endTimeIndex = 0;
        
//        for (int i = 0; i < startTimeBox.getItems().size(); i++) {
//            if (!startTimeBox.getItems().get(i).equals(startTime))
//                startTimeBox.getItems().remove(i);
//        }
//        for (int i = 0; i < endTimeBox.getItems().size(); i++) {
//            if (!endTimeBox.getItems().get(i).equals(endTime))
//                endTimeBox.getItems().remove(i);
//        }
        startTimeBox.getItems().clear();
        endTimeBox.getItems().clear();
        for (int i = 0; i < startTimeComboBox.length; i++) {
            if (startTime.equals(startTimeComboBox[i])) 
                startTimeIndex = i;
        }
         for (int i = 0; i < endTimeComboBox.length; i++) {
           if (endTime.equals(endTimeComboBox[i]))
                endTimeIndex = i;
        }
         
         for (int i = 0; i <= endTimeIndex; i++) {
//            if (i <= endTimeIndex && !startTimeBox.getItems().contains(startTimeComboBox[i]))
                startTimeBox.getItems().add(startTimeComboBox[i]);
        }
         for (int i = startTimeIndex; i < endTimeComboBox.length; i++) {
//            if (i >= startTimeIndex && !endTimeBox.getItems().contains(endTimeComboBox[i]))
                endTimeBox.getItems().add(endTimeComboBox[i]);
        }
         startTimeBox.setValue(startTime);
         endTimeBox.setValue(endTime);
//         startTimeBox.getSelectionModel().select(startTime);
//         endTimeBox.getSelectionModel().select(endTime);
                 
//        for (int i = 0; i < startTimeComboBox.length; i++) {
//            if (i < startTimeIndex && endTimeBox.getItems().contains(startTimeComboBox[i]))
//                endTimeBox.getItems().remove(startTimeComboBox[i]);
//            else if (i > startTimeIndex && !startTimeBox.getItems().contains(startTimeComboBox[i]))
//                startTimeBox.getItems().add(startTimeComboBox[i]);
//        }
//        for (int i = 0; i < endTimeComboBox.length; i++) {
//            if (i > endTimeIndex && startTimeBox.getItems().contains(endTimeComboBox[i]))
//                startTimeBox.getItems().remove(endTimeComboBox[i]);
//            else if (i < endTimeIndex && !endTimeBox.getItems().contains(endTimeComboBox[i]))
//                endTimeBox.getItems().add(endTimeComboBox[i]);
//        }
    }
}
