/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author New User
 */
public class MeetingTimes {
    StringProperty section;
    StringProperty days;
    StringProperty time;
    StringProperty room;
    StringProperty daysAndTime;
    StringProperty ta1;
    StringProperty ta2;
    public MeetingTimes(String initSection, String initDays, String initTime, String initRoom, String initDaysAndTime, 
                String initTa1, String initTa2) {
        this.section = new SimpleStringProperty(initSection);
        this.days = new SimpleStringProperty(initDays);
        this.time = new SimpleStringProperty(initTime);
        this.room = new SimpleStringProperty(initRoom);
        this.daysAndTime = new SimpleStringProperty(initDaysAndTime);
        this.ta1 = new SimpleStringProperty(initTa1);
        this.ta2 = new SimpleStringProperty(initTa2);
        
    }
    
    public void setSection(String initSection) {
        this.section = new SimpleStringProperty(initSection);
    }
    public String getSection() {
        return section.get();
    }
    public void setDays(String initDays) {
        this.days = new SimpleStringProperty(initDays);
    }
    public String getDays() {
        return days.get();
    }
    public void setTime(String initTime) {
        this.time = new SimpleStringProperty(initTime);
    }
    public String getTime() {
        return time.get();
    }
    public void setRoom(String initRoom) {
        this.room = new SimpleStringProperty(initRoom);
    }
    public String getRoom() {
        return room.get();
    }
    public void setDaysAndTime(String initDaysAndTime) {
        this.daysAndTime = new SimpleStringProperty(initDaysAndTime);
    }
    public String getDaysAndTime() {
        return daysAndTime.get();
    }
    public void setTa1(String initTa1) {
        this.ta1 = new SimpleStringProperty(initTa1);
    }
    public String getTa1() {
        return ta1.get();
    }
     public void setTa2(String initTa2) {
        this.ta2 = new SimpleStringProperty(initTa2);
    }
    public String getTa2() {
        return ta2.get();
    }
}
